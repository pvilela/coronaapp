package coronaapp;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class CoronaApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //model.PacienteDAO.getInstance().create("Plinio Vilela", "(19) 97070-7070", "344.443.334-34", "Rua da Bobos, 0", "senha", false);
        //model.PacienteDAO.getInstance().create("Spiderman", "(19) 91010-1234", "345.443.334-34", "Madson Square Garden 100", "maryjane", true);
        
        
        for(model.Paciente paciente : model.PacienteDAO.getInstance().retrieveAll()){
            System.out.println(paciente);
        }
        
        // Funcional Operator:
//        model.PacienteDAO.getInstance().retrieveAll().forEach((paciente) -> {
//            System.out.println(paciente);
//        });
        
        
    }
    
}
