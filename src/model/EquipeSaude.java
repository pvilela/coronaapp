package model;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class EquipeSaude {
    private int codigo;
    private String base;

    public EquipeSaude(int codigo, String base) {
        this.codigo = codigo;
        this.base = base;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getBase() {
        return base;
    }
    
}
