package model;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class Mensagem {
    private Paciente paciente;
    private EquipeSaude equipeSaude;
    private String conteudo;

    public Mensagem(Paciente paciente, EquipeSaude equipeSaude, String conteudo) {
        this.paciente = paciente;
        this.equipeSaude = equipeSaude;
        this.conteudo = conteudo;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public EquipeSaude getEquipeSaude() {
        return equipeSaude;
    }

    public String getConteudo() {
        return conteudo;
    }
    
}
