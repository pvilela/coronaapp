package model;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class Paciente {
    private int id;
    private String nome;
    private String telefone;
    private String CPF;
    private String endereco;
    private String senha;
    private boolean isInfectado;

    public Paciente(int id, String nome, String telefone, String CPF, String endereco, String senha, boolean isInfectado) {
        this.id = id;
        this.nome = nome;
        this.telefone = telefone;
        this.CPF = CPF;
        this.endereco = endereco;
        this.senha = senha;
        this.isInfectado = isInfectado;
    }
    
    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCPF() {
        return CPF;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getSenha() {
        return senha;
    }

    public boolean isInfectado() {
        return isInfectado;
    }

    @Override
    public String toString() {
        return "Paciente{" + "id= " + id + ", nome= " + nome + ", telefone= " + telefone + ", CPF= " + CPF + ", endereco= " + endereco + ", senha= " + senha + ", isInfectado= " + isInfectado + '}';
    }
    
}
