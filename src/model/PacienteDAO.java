package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class PacienteDAO extends DAO {

    private static PacienteDAO instance;

    private PacienteDAO() {
        DAO.getConnection();
    }

    public static PacienteDAO getInstance() {
        if (instance == null) {
            instance = new PacienteDAO();
        }
        return instance;
    }

    // CRUD
    public void create(String nome, String telefone, String CPF, String endereco, String senha, boolean isInfectado) {
        PreparedStatement stmt;
        try {
            int newId = getLastId() + 1;
            stmt = DAO.getConnection().prepareStatement("INSERT INTO paciente (id, nome, telefone, CPF, endereco, senha, isInfectado) VALUES (?,?,?,?,?,?,?)");
            stmt.setInt(1, newId);
            stmt.setString(2, nome);
            stmt.setString(3, telefone);
            stmt.setString(4, CPF);
            stmt.setString(5, endereco);
            stmt.setString(6, senha);
            stmt.setBoolean(7, isInfectado);
            executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Paciente buildObject(ResultSet rs) {
        Paciente paciente = null;
        try {
            paciente = new Paciente(rs.getInt("id"), rs.getString("nome"), rs.getString("telefone"), rs.getString("CPF"), rs.getString("endereco"), rs.getString("senha"), rs.getBoolean("isInfectado"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return paciente;
    }

    public Paciente retrieveByPrimaryKey(int id) {
        Paciente result = null;
        ResultSet rs = null;
        rs = getResultSet("SELECT * FROM paciente WHERE id=" + id);
        try {
            if (rs.next()) {
                result = buildObject(rs);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Paciente> retrieveAll() {
        ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
        ResultSet rs = null;
        rs = getResultSet("SELECT * FROM paciente");
        try {
            while (rs.next()) {
                pacientes.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pacientes;
    }

    public Paciente retrieveByName(String name) {
        Paciente result = null;
        ResultSet rs = null;
        rs = getResultSet("SELECT * FROM paciente WHERE nome='" + name + "'");
        try {
            if (rs.next()) {
                result = buildObject(rs);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean update(Paciente paciente) {
        PreparedStatement stmt;
        try {
            stmt = DAO.getConnection().prepareStatement("UPDATE paciente SET nome=?, telefone=?, cpf=?, endereco=?, senha=?, isinfectado=? WHERE id = ?");
            stmt.setString(1, paciente.getNome());
            stmt.setString(2, paciente.getTelefone());
            stmt.setString(3, paciente.getCPF());
            stmt.setString(4, paciente.getEndereco());
            stmt.setString(5, paciente.getSenha());
            stmt.setBoolean(6, paciente.isInfectado());
            int update = executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Paciente paciente) {
        PreparedStatement stmt;
        try {
            stmt = DAO.getConnection().prepareStatement("DELETE FROM paciente WHERE id = ?");
            stmt.setInt(1, paciente.getId());
            executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getLastId() {
        return lastId("paciente", "id");
    }
}//PacienteDAO

