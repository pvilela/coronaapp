package model;

import java.util.Calendar;

/**
 * @author Plinio Vilela - prvilela@unicamp.br
 * @date April 13, 2020
 */
public class Visita {
    private Paciente paciente;
    private Calendar dataDaVisita;
    private boolean isRealizada;
    private EquipeSaude equipeSaude;

    public Visita(Paciente paciente, Calendar dataDaVisita, boolean isRealizada, EquipeSaude equipeSaude) {
        this.paciente = paciente;
        this.dataDaVisita = dataDaVisita;
        this.isRealizada = isRealizada;
        this.equipeSaude = equipeSaude;
    }
    
    public Paciente paciente() {
        return paciente;
    }

    public Calendar getDataDaVisita() {
        return dataDaVisita;
    }

    public void setDataDaVisita(Calendar dataDaVisita) {
        this.dataDaVisita = dataDaVisita;
    }

    public boolean isIsRealizada() {
        return isRealizada;
    }

    public void setIsRealizada(boolean isRealizada) {
        this.isRealizada = isRealizada;
    }
    
}
